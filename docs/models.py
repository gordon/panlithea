from wagtail.core.models import Page
from wagtail.core.fields import StreamField
from wagtail.admin.edit_handlers import StreamFieldPanel
from wagtail.search import index

from .blocks import SectionBlock


class DocPage(Page):
    body = StreamField([
        ('section', SectionBlock()),
    ])

    content_panels = Page.content_panels + [
        StreamFieldPanel('body'),
    ]

    search_fields = Page.search_fields + [
        index.SearchField('body'),
    ]

    subpage_types = ['docs.DocPage']

    def get_context(self, request):
        context = super().get_context(request)
        return context

    def get_subpages(self):
        children = self.get_children()
        return children.live().in_menu()
