from django.utils.translation import get_language
from .models import IconLink, Script, MetaTag, FooterLink


def snippets(request):
    lang = get_language()
    site = request.site
    context = {
        'icons': IconLink.objects.filter(locale__language_code=lang, site=site),
        'scripts': Script.objects.filter(locale__language_code=lang, site=site),
        'meta_tags': MetaTag.objects.filter(locale__language_code=lang, site=site),
        'footer_links': FooterLink.objects.filter(locale__language_code=lang, site=site),
    }
    return context
