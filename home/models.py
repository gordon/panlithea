from datetime import date

from django.db import models
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from django.conf import settings
from django.utils import timezone
from django.utils.translation import gettext as _

from wagtail.core.models import Page, Orderable, TranslatableMixin, BootstrapTranslatableMixin
from wagtail.core import blocks
from wagtail.core.fields import RichTextField, StreamField
from wagtail.images.edit_handlers import ImageChooserPanel
from wagtail.documents.edit_handlers import DocumentChooserPanel
from wagtail.admin.edit_handlers import (
    FieldPanel, StreamFieldPanel, PageChooserPanel,
    FieldRowPanel, InlinePanel, MultiFieldPanel, TabbedInterface, ObjectList
)
from wagtail.images.blocks import ImageChooserBlock
from modelcluster.fields import ParentalKey
from wagtail.contrib.forms.models import AbstractEmailForm, AbstractFormField
from wagtail.search import index
from wagtail.snippets.models import register_snippet

from wagtailmarkdown.blocks import MarkdownBlock

from .blocks import (
    H2Block, H3Block, IllustratedParagraphBlock, BlockQuoteBlock,
    NavigationBlock, ContactInfoBlock, ContactIconListBlock
)
from .forms import FormBuilder


META_CHOICES = (
    ('shortcut-icon', 'Shortcut icon'),
    ('twitter:site', 'Twitter account'),
    ('og:locale', 'OpenGraph locale'),
    ('og:description', 'OpenGraph description'),
    ('og:image', 'OpenGraph image'),
)


class HomePage(Page):
    intro = RichTextField(blank=True)
    body = StreamField([
        ('h2', H2Block()),
        ('h3', H3Block()),
        ('paragraph', blocks.RichTextBlock()),
        ('markdown', MarkdownBlock(icon='code')),
        ('quote', BlockQuoteBlock()),
        ('illustrated_paragraph', IllustratedParagraphBlock()),
    ])
    blog_page = models.ForeignKey('home.BlogIndexPage',
                                  on_delete=models.SET_NULL, null=True,
                                  blank=True, related_name='+')


    content_panels = Page.content_panels + [
        FieldPanel('intro', classname='full'),
        StreamFieldPanel('body'),
        PageChooserPanel('blog_page'),
    ]

    edit_handler = TabbedInterface([
        ObjectList(content_panels, heading=_('Content')),
        ObjectList(Page.promote_panels, heading=_('Promote')),
        ObjectList(Page.settings_panels, heading=_('Settings'),
                   classname="settings"),
    ])

    search_fields = Page.search_fields + [
        index.SearchField('intro'),
        index.SearchField('body'),
    ]

    def get_context(self, request):
        context = super().get_context(request)
        # import ipdb
        # ipdb.set_trace()
        context['home_page'] = True
        return context


class BlogIndexPage(Page):
    intro = RichTextField(blank=True)

    content_panels = Page.content_panels + [
        FieldPanel('intro', classname='full'),
    ]

    parent_page_types = ['home.HomePage']
    subpage_types = ['home.BlogPage']

    search_fields = Page.search_fields + [
        index.SearchField('intro'),
    ]

    def get_first_children(self):
        return self.get_children().live().order_by('-blogpage__date')[:3]

    def get_context(self, request):
        context = super().get_context(request)
        children = self.get_children().order_by('-blogpage__date', '-first_published_at')
        paginator = Paginator(children, settings.BLOG_ITEMS_PER_PAGE)
        page = request.GET.get('page')
        try:
            context['articles'] = paginator.page(page)
        except PageNotAnInteger:
            context['articles'] = paginator.page(1)
        except EmptyPage:
            context['articles'] = paginator.page(paginator.num_pages)
        return context


class BlogPage(Page):
    date = models.DateField('Post date', default=date.today)
    intro = models.CharField(max_length=250)
    body = StreamField([
        ('h2', H2Block()),
        ('h3', H3Block()),
        ('paragraph', blocks.RichTextBlock()),
        ('markdown', MarkdownBlock(icon='code')),
        ('quote', BlockQuoteBlock()),
        ('illustrated_paragraph', IllustratedParagraphBlock()),
        ('image', ImageChooserBlock()),
    ])
    image = models.ForeignKey('wagtailimages.Image', on_delete=models.PROTECT,
                              related_name='+', null=True)

    content_panels = Page.content_panels + [
        FieldPanel('date'),
        FieldPanel('intro'),
        ImageChooserPanel('image'),
        StreamFieldPanel('body'),
    ]

    search_fields = Page.search_fields + [
        index.SearchField('intro'),
        index.SearchField('body'),
        index.FilterField('date'),
    ]

    parent_page_types = ['home.BlogIndexPage']
    subpage_types = []

    @property
    def feed_image(self):
        return self.image


class StandardPage(Page):
    file = models.ForeignKey('wagtaildocs.Document',
                             on_delete=models.PROTECT, related_name='+',
                             null=True, blank=True)
    body = StreamField([
        ('h2', H2Block()),
        ('h3', H3Block()),
        ('paragraph', blocks.RichTextBlock()),
        ('markdown', MarkdownBlock(icon='code')),
        ('quote', BlockQuoteBlock()),
        ('illustrated_paragraph', IllustratedParagraphBlock()),
        ('image', ImageChooserBlock()),
    ])

    content_panels = Page.content_panels + [
        StreamFieldPanel('body'),
        DocumentChooserPanel('file'),
    ]

    search_fields = Page.search_fields + [
        index.SearchField('body'),
    ]


class FormField(AbstractFormField):
    page = ParentalKey('FormPage', on_delete=models.CASCADE,
                       related_name='form_fields')
    css_class = models.CharField(max_length=30, blank=True)

    panels = AbstractFormField.panels + [
        FieldPanel('css_class'),
    ]


class FormPage(AbstractEmailForm):

    form_builder = FormBuilder

    intro = StreamField([
        ('h2', H2Block()),
        ('h3', H3Block()),
        ('paragraph', blocks.RichTextBlock()),
        ('markdown', MarkdownBlock(icon='code')),
        ('quote', BlockQuoteBlock()),
        ('illustrated_paragraph', IllustratedParagraphBlock()),
        ('image', ImageChooserBlock()),
    ])
    thank_you_text = StreamField([
        ('h2', H2Block()),
        ('h3', H3Block()),
        ('paragraph', blocks.RichTextBlock()),
        ('quote', BlockQuoteBlock()),
        ('illustrated_paragraph', IllustratedParagraphBlock()),
        ('image', ImageChooserBlock()),
    ])

    content_panels = AbstractEmailForm.content_panels + [
        StreamFieldPanel('intro'),
        InlinePanel('form_fields', label="Form fields"),
        StreamFieldPanel('thank_you_text'),
        MultiFieldPanel([
            FieldRowPanel([
                FieldPanel('from_address', classname="col6"),
                FieldPanel('to_address', classname="col6"),
            ]),
            FieldPanel('subject'),
        ], "Email"),
    ]

    search_fields = AbstractEmailForm.search_fields + [
        index.SearchField('intro'),
    ]

    parent_page_types = ['home.HomePage']
    subpage_types = []


@register_snippet
class MetaTag(TranslatableMixin, models.Model):
    meta_type = models.CharField(max_length=30, choices=META_CHOICES)
    content = models.CharField(max_length=128, blank=True)
    image = models.ForeignKey('wagtailimages.Image', on_delete=models.PROTECT,
                              related_name='+', null=True, blank=True)
    site = models.ForeignKey('wagtailcore.Site', on_delete=models.PROTECT,
                             related_name='metatags')
    ordering = models.PositiveSmallIntegerField(default=0)

    panels = [
        FieldPanel('meta_type'),
        FieldPanel('content'),
        ImageChooserPanel('image'),
        FieldPanel('site'),
        FieldPanel('ordering'),
    ]

    def __str__(self):
        return '#{} {}: {}'.format(self.ordering, self.site, self.meta_type)

    class Meta(TranslatableMixin.Meta):
        ordering = ['ordering']


@register_snippet
class Script(TranslatableMixin, models.Model):
    css_file = models.ForeignKey('wagtaildocs.Document',
                                 on_delete=models.PROTECT, related_name='+',
                                 null=True, blank=True)
    js_file = models.ForeignKey('wagtaildocs.Document',
                                on_delete=models.PROTECT, related_name='+',
                                null=True, blank=True)
    script_content = models.TextField(blank=True)
    site = models.ForeignKey('wagtailcore.Site', on_delete=models.PROTECT,
                             related_name='scripts')
    ordering = models.PositiveSmallIntegerField(default=0)

    panels = [
        DocumentChooserPanel('css_file'),
        DocumentChooserPanel('js_file'),
        FieldPanel('script_content', classname='full'),
        FieldPanel('site'),
        FieldPanel('ordering'),
    ]

    def __str__(self):
        return '#{} {}: {}'.format(self.ordering, self.site, self.js_file)

    class Meta(TranslatableMixin.Meta):
        ordering = ['ordering']


@register_snippet
class FooterLink(TranslatableMixin, Orderable, models.Model):
    title = models.CharField(max_length=50)
    page = models.ForeignKey('wagtailcore.Page', on_delete=models.CASCADE,
                             related_name='+')
    site = models.ForeignKey('wagtailcore.Site', on_delete=models.PROTECT,
                             related_name='footer_links')
    ordering = models.PositiveSmallIntegerField(default=0)

    panels = [
        FieldPanel('title'),
        PageChooserPanel('page'),
        FieldPanel('site'),
        FieldPanel('ordering'),
    ]

    def __str__(self):
        return '#{} {}: {}'.format(self.ordering, self.site, self.title)

    class Meta(TranslatableMixin.Meta):
        ordering = ['ordering']


@register_snippet
class IconLink(TranslatableMixin, Orderable, models.Model):
    title = models.CharField(max_length=50)
    url = models.URLField()
    icon = models.CharField(max_length=50)
    site = models.ForeignKey('wagtailcore.Site', on_delete=models.PROTECT,
                             related_name='icon_links')
    ordering = models.PositiveSmallIntegerField(default=0)

    panels = [
        FieldPanel('title'),
        FieldPanel('url'),
        FieldPanel('icon'),
        FieldPanel('site'),
        FieldPanel('ordering'),
    ]

    def __str__(self):
        return '#{} {}: {}'.format(self.ordering, self.title, self.url)

    class Meta(TranslatableMixin.Meta):
        ordering = ['ordering']
