# Generated by Django 2.0.4 on 2018-04-16 14:58

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('home', '0007_featureindexpage_featurepage'),
    ]

    operations = [
        migrations.AlterField(
            model_name='homepage',
            name='features_page',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.SET_NULL, related_name='+', to='home.FeatureIndexPage'),
        ),
    ]
