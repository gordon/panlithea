# Generated by Django 2.0.4 on 2018-04-16 22:26

from django.db import migrations
import home.blocks
import wagtail.core.blocks
import wagtail.core.fields
import wagtail.images.blocks


class Migration(migrations.Migration):

    dependencies = [
        ('home', '0019_auto_20180416_2225'),
    ]

    operations = [
        migrations.AlterField(
            model_name='blogpage',
            name='body',
            field=wagtail.core.fields.StreamField((('h2', home.blocks.H2Block()), ('h3', home.blocks.H3Block()), ('paragraph', wagtail.core.blocks.RichTextBlock()), ('quote', wagtail.core.blocks.StructBlock((('text', wagtail.core.blocks.RichTextBlock()), ('credit', wagtail.core.blocks.RichTextBlock(required=False))))), ('illustrated_paragraph', wagtail.core.blocks.StructBlock((('text', wagtail.core.blocks.RichTextBlock()), ('image', wagtail.images.blocks.ImageChooserBlock()), ('image_position', wagtail.core.blocks.ChoiceBlock(choices=[('left', 'Float left'), ('right', 'Float right')]))))), ('image', wagtail.images.blocks.ImageChooserBlock()))),
        ),
        migrations.AlterField(
            model_name='featurepage',
            name='body',
            field=wagtail.core.fields.StreamField((('h2', home.blocks.H2Block()), ('h3', home.blocks.H3Block()), ('paragraph', wagtail.core.blocks.RichTextBlock()), ('quote', wagtail.core.blocks.StructBlock((('text', wagtail.core.blocks.RichTextBlock()), ('credit', wagtail.core.blocks.RichTextBlock(required=False))))), ('illustrated_paragraph', wagtail.core.blocks.StructBlock((('text', wagtail.core.blocks.RichTextBlock()), ('image', wagtail.images.blocks.ImageChooserBlock()), ('image_position', wagtail.core.blocks.ChoiceBlock(choices=[('left', 'Float left'), ('right', 'Float right')]))))), ('image', wagtail.images.blocks.ImageChooserBlock()))),
        ),
        migrations.AlterField(
            model_name='formpage',
            name='intro',
            field=wagtail.core.fields.StreamField((('h2', home.blocks.H2Block()), ('h3', home.blocks.H3Block()), ('paragraph', wagtail.core.blocks.RichTextBlock()), ('quote', wagtail.core.blocks.StructBlock((('text', wagtail.core.blocks.RichTextBlock()), ('credit', wagtail.core.blocks.RichTextBlock(required=False))))), ('illustrated_paragraph', wagtail.core.blocks.StructBlock((('text', wagtail.core.blocks.RichTextBlock()), ('image', wagtail.images.blocks.ImageChooserBlock()), ('image_position', wagtail.core.blocks.ChoiceBlock(choices=[('left', 'Float left'), ('right', 'Float right')]))))), ('image', wagtail.images.blocks.ImageChooserBlock()))),
        ),
        migrations.AlterField(
            model_name='formpage',
            name='thank_you_text',
            field=wagtail.core.fields.StreamField((('h2', home.blocks.H2Block()), ('h3', home.blocks.H3Block()), ('paragraph', wagtail.core.blocks.RichTextBlock()), ('quote', wagtail.core.blocks.StructBlock((('text', wagtail.core.blocks.RichTextBlock()), ('credit', wagtail.core.blocks.RichTextBlock(required=False))))), ('illustrated_paragraph', wagtail.core.blocks.StructBlock((('text', wagtail.core.blocks.RichTextBlock()), ('image', wagtail.images.blocks.ImageChooserBlock()), ('image_position', wagtail.core.blocks.ChoiceBlock(choices=[('left', 'Float left'), ('right', 'Float right')]))))), ('image', wagtail.images.blocks.ImageChooserBlock()))),
        ),
        migrations.AlterField(
            model_name='homepage',
            name='body',
            field=wagtail.core.fields.StreamField((('h2', home.blocks.H2Block()), ('h3', home.blocks.H3Block()), ('paragraph', wagtail.core.blocks.RichTextBlock()), ('quote', wagtail.core.blocks.StructBlock((('text', wagtail.core.blocks.RichTextBlock()), ('credit', wagtail.core.blocks.RichTextBlock(required=False))))), ('illustrated_paragraph', wagtail.core.blocks.StructBlock((('text', wagtail.core.blocks.RichTextBlock()), ('image', wagtail.images.blocks.ImageChooserBlock()), ('image_position', wagtail.core.blocks.ChoiceBlock(choices=[('left', 'Float left'), ('right', 'Float right')]))))), ('image', wagtail.images.blocks.ImageChooserBlock()))),
        ),
    ]
