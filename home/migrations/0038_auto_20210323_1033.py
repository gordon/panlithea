# Generated by Django 3.1.7 on 2021-03-23 09:33

from django.db import migrations
from wagtail.core.models import BootstrapTranslatableModel


class Migration(migrations.Migration):

    dependencies = [
        ('home', '0037_auto_20210323_1033'),
    ]

    operations = [
        BootstrapTranslatableModel('home.MetaTag'),
        BootstrapTranslatableModel('home.Script'),
        BootstrapTranslatableModel('home.FooterLink'),
        BootstrapTranslatableModel('home.IconLink'),
    ]
